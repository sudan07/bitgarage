from django.db.models.signals import post_save
from django.contrib.auth.models import User
from django.contrib.auth.models import Group

from .models import Worker

def  worker_profile(sender,instance,created,**kwargs):
    if created:
        group=Group.objects.get(name='worker')   
        instance.groups.add(group)
        Worker.objects.create(
            user=instance,
            name=instance.username,
            email=instance.email

        )

post_save.connect(worker_profile, sender=User)




