from django.shortcuts import render,redirect
from django.contrib.auth import login
from django.http import HttpResponse
from django.contrib.auth.forms import UserCreationForm
from .models import Task,Goods,Sells,Worker,Custumer,Product
from .forms import CreateUserForm,TaskForm,GoodsForm,SellsForm
from django.contrib.auth.models import Group

from django.contrib.auth import authenticate,login,logout
from .decorators import admin_only,allowed_users,unauthenicated_user
from django.contrib.auth.decorators import login_required
from django.shortcuts import render_to_response
from django.db.models import Sum


# Create your views here.


@login_required(login_url='login')
@allowed_users(allowed_roles=['admin'])
def register(request):
    # if request.user.is_authenticated:
    #     return redirect('home')
    # else:
        form= CreateUserForm()

        if request.method == 'POST':
            form=CreateUserForm(request.POST)
            if form.is_valid():
                user=form.save()
                username=form.cleaned_data.get('username')
                email=form.cleaned_data.get('email')


                return redirect('login')
        
        context={'form':form}
        return render(request , 'register.html', context)

@unauthenicated_user
def loginpage(request):
    if request.method=='POST':
        username=request.POST.get("username")
        password=request.POST.get("password")

        user= authenticate(request,username=username, password=password)

        if user is not None:
            login(request,user)
            return redirect('home')
    
    context={}
    return render(request,'login.html')


def logoutuser(request):
    logout(request)
    return redirect('login')

@login_required(login_url='login')
# @allowed_users(allowed_roles=['admin'])
@admin_only
def home(request):
    sold=Sells.objects.all().count()
    goods=Goods.objects.all().count()
    custumer=Custumer.objects.all().count()
    worker=Worker.objects.all().count()
    product=Goods.objects.all()
    sells=Sells.objects.aggregate(due=Sum('dueamount'),paid=Sum('paidamount'))
    print(sells)
    context={'sold':sold,'goods':goods,'custumer':custumer,'worker':worker,'product':product,'sells':sells}
    return render(request,'index.html',context)

# @allowed_users(allowed_roles=['admin'])
@allowed_users(allowed_roles=['worker'])
# @admin_only
def workerpage(request):
    task=request.user.worker.task_set.all()
   
    context={'task':task}
    return render(request,'worker.html',context)

@allowed_users(allowed_roles=['accountant'])
def accountantpage(request):
    return render(request,'accountant.html')



# def taskpage(request):
#     return render(request,'task.html')

def notificationpage(request):
    sells=Sells.objects.all()
    context={'sells':sells}
    return render(request,'notification.html',context)

def profile(request):
    
    return render(request,'userprofile.html')

def connection(request):
    return render(request,'new_connection.html')

def taskpage(request):
    return render(request,'task.html')



def task(request):
    tasks=Task.objects.all()
    form= TaskForm()

    if request.method == 'POST':
        form=TaskForm(request.POST)
        if form.is_valid():
            form.save()

            print('Tasksubmitted')

            
    contexts={'form':form,'tasks':tasks}
    return render(request , 'task.html', contexts)





def update(request,pk):
    pass 


def delete(request, pk):
    task = Task.objects.get(id=pk)
    if request.method == 'POST':
        task.delete()
        return redirect('task-page')

    context ={'item':task}
    return render(request,'delete.html', context)





def pasalpage(request):
    
    goods=Goods.objects.all()
    sells=Sells.objects.all()
    context={'goods':goods,'sells':sells}
    return render(request , 'pasal.html',context)

# def sells(request):
#     Sale=Sells.objects.all()
#     sells=SellsForm()
#     if request.method == 'POST':
#         sells=SellsForm(request.POST)

#         if sells.is_valid():
#             form.save()

#             return redirect('pasal-page')

#     context={'sells':sells,'Sale':Sale}
#     return render(request , 'pasal.html',context)
def sells(request):
    form=SellsForm()
    if request.method == 'POST':
       form=SellsForm(request.POST)
       if form.is_valid():
           form.save()
           return redirect('pasal-page')
    
    context={'form':form}
    
    return render(request,'sells.html',context)


# def addgoods(request):
#     return render(request,'addgoods.html')



def addgoods(request):
    
    goods=Goods.objects.all()
    form=GoodsForm()
    
    if request.method == 'POST':
       form=GoodsForm(request.POST)
       
      
       if form.is_valid():
           form.save()
           return redirect('pasal-page')
    
    context={'form':form,'goods':goods}
    return render(request , 'addgoods.html',context)


def test(request):
    """
    pieChart page
    """
    xdata = ["Apple", "Apricot", "Avocado", "Banana", "Boysenberries", "Blueberries", "Dates", "Grapefruit", "Kiwi", "Lemon"]
    ydata = [52, 48, 160, 94, 75, 71, 490, 82, 46, 17]

    extra_serie = {"tooltip": {"y_start": "", "y_end": " cal"}}
    chartdata = {'x': xdata, 'y1': ydata, 'extra1': extra_serie}
    charttype = "pieChart"

    data = {
        'charttype': charttype,
        'chartdata': chartdata,
    }
    return render_to_response('test.html', data)

def layout(request):
    sold=Sells.objects.all().count()
    goods=Goods.objects.all().count()
    custumer=Custumer.objects.all().count()
    worker=Worker.objects.all().count()
    context={'sold':sold,'goods':goods,'custumer':custumer,'worker':worker}
    return render(request,'layout.html',context)
    