from . import views
from django.urls import path
# from . import employee


urlpatterns = [
    path('', views.home, name='home'),
    path('login/',views.loginpage,name='login'),
    path('register/',views.register, name='register'),
    path('logout/',views.logoutuser, name='logout'),
    path('profile/',views.profile ,name='profile'),
    path('worker/',views.workerpage,name='worker-page'),
    path('accountant/',views.accountantpage,name='accountant-page'),
    path('pasal/',views.pasalpage ,name='pasal-page'),
    # path('task',views.taskpage ,name='task-page'),
    path('notification',views.notificationpage ,name='notification-page'),
    path('worker/newconnection',views.connection , name='newconnection'),
    path('task/',views.task ,name='task-page'),
    path('pasal/sells/',views.sells ,name='test'),
    path('update/<str:pk>/',views.update , name='update'),
    path('delete/<str:pk>/', views.delete, name='delete'),
    path('pasal/addgoods',views.addgoods ,name='add-goods'),
    path('madan/',views.test, name='tests'),
    path('layout',views.layout,name='layout')

]