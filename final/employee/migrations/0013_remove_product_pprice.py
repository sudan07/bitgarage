# Generated by Django 2.2.7 on 2020-04-13 14:39

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('employee', '0012_remove_sells_quantity'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='product',
            name='pprice',
        ),
    ]
