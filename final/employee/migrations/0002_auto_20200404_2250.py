# Generated by Django 2.2.7 on 2020-04-04 17:05

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('employee', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sells',
            name='productname',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='employee.Product'),
        ),
    ]
