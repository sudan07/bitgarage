# Generated by Django 2.2.7 on 2020-04-12 04:51

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('employee', '0009_sells_quantity'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='pprice',
            field=models.IntegerField(default=0),
            preserve_default=False,
        ),
    ]
