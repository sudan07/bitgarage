from django.db import models
from django.contrib.auth.models import User
from multiselectfield import MultiSelectField


# Create your models here.
class Worker(models.Model):
    user=models.OneToOneField(User , null=False , on_delete=models.CASCADE)
    name=models.CharField(max_length=60,null=False)
    email=models.CharField(max_length=60 ,null=False)


    def __str__(self):
        return self.name

tasks=(
    ("Water pouring","Water pouring"),
    ("Solar","Solar"),
    ("geyser","geyser"),
)

taskmode=(
    ("Pending","Pending"),
    ("Completed","Completed"),
)

class Custumer(models.Model):
    custumername=models.CharField(max_length=50)
    address=models.CharField(max_length=50)
    phone=models.CharField(max_length=50)

    def __str__(self):
        return self.custumername

    
class Product(models.Model):
    productname=models.CharField(max_length=50,unique=True)
    # pprice=models.IntegerField()

    def __str__(self):
        return self.productname


class Task(models.Model):
    assignedto=models.ForeignKey(Worker,null=True , on_delete=models.CASCADE)
    custumername= models.ForeignKey(Custumer, on_delete=models.CASCADE)
    phone = models.CharField(max_length=60)
    task = MultiSelectField(max_length=255,choices=tasks)
    assignedby = models.CharField(max_length=50)
    taskaction=models.CharField(max_length=50,choices=taskmode,default=taskmode[0][0])

    def __str__(self):
        return self.assignedby



class Goods(models.Model):
    productname=models.OneToOneField(Product,on_delete=models.CASCADE,unique=True)
    amount=models.IntegerField()
    quantity=models.IntegerField(default=1)
    total=models.IntegerField()

    def save(self,*args,**kwargs):
        self.total=self.amount * self.quantity
        super(Goods,self).save(*args,**kwargs)

class Sells(models.Model):
    custumername= models.ForeignKey(Custumer, on_delete=models.CASCADE)
    productname=models.ForeignKey(Product,on_delete=models.CASCADE)
    # quantity=models.IntegerField(default=0)
    amount=models.IntegerField()
    paidamount=models.IntegerField()
    dueamount=models.IntegerField()
    assignedby=models.CharField(max_length=50)

    def save(self,*args,**kwargs):
        self.dueamount=self.amount-self.paidamount
        super(Sells,self).save(*args,**kwargs)


  


