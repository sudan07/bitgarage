from django.db import models
from django.contrib.auth.models import User
from multiselectfield import MultiSelectField


# Create your models here.
class Worker(models.Model):
    user=models.OneToOneField(User , null=False , on_delete=models.CASCADE)
    name=models.CharField(max_length=60,null=False)
    email=models.CharField(max_length=60 ,null=False)


    def __str__(self):
        return self.name

tasks=(
    ("Water pouring","Water pouring"),
    ("Solar","Solar"),
    ("geyser","geyser"),
)


class Task(models.Model):
    assignedto=models.OneToOneField(Worker,null=True , on_delete=models.CASCADE)
    custumername = models.CharField(max_length=60)
    phone = models.CharField(max_length=60)
    task = MultiSelectField(max_length=255,choices=tasks)
    assignedby = models.CharField(max_length=50)

    def __str__(self):
        return self.custumername



class Goods(models.Model):
    productname=models.CharField(max_length=50)
    amount=models.IntegerField()
    quantity=models.IntegerField(default=1)
    total=models.IntegerField(default=0)

    def __str__(self):
        return self.productname
