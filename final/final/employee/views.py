from django.shortcuts import render,redirect
from django.contrib.auth import login
from django.http import HttpResponse
from django.contrib.auth.forms import UserCreationForm
from .models import Task,Goods
from .forms import CreateUserForm,TaskForm,GoodsForm
from django.contrib.auth.models import Group

from django.contrib.auth import authenticate,login,logout
from .decorators import admin_only,allowed_users,unauthenicated_user
from django.contrib.auth.decorators import login_required


# Create your views here.


@login_required(login_url='login')
@allowed_users(allowed_roles=['admin'])
def register(request):
    # if request.user.is_authenticated:
    #     return redirect('home')
    # else:
        form= CreateUserForm()

        if request.method == 'POST':
            form=CreateUserForm(request.POST)
            if form.is_valid():
                user=form.save()
                username=form.cleaned_data.get('username')
                email=form.cleaned_data.get('email')


                return redirect('login')
        
        context={'form':form}
        return render(request , 'register.html', context)

@unauthenicated_user
def loginpage(request):
    if request.method=='POST':
        username=request.POST.get("username")
        password=request.POST.get("password")

        user= authenticate(request,username=username, password=password)

        if user is not None:
            login(request,user)
            return redirect('home')
    
    context={}
    return render(request,'login.html')


def logoutuser(request):
    logout(request)
    return redirect('login')

@login_required(login_url='login')
# @allowed_users(allowed_roles=['admin'])
@admin_only
def home(request):
    return render(request,'index.html')

# @allowed_users(allowed_roles=['admin'])
@allowed_users(allowed_roles=['worker'])
# @admin_only
def workerpage(request):
    return render(request,'worker.html')

@allowed_users(allowed_roles=['accountant'])
def accountantpage(request):
    return render(request,'accountant.html')



# def taskpage(request):
#     return render(request,'task.html')

def notificationpage(request):
    return render(request,'notification.html')

def profile(request):
    return render(request,'userprofile.html')

def connection(request):
    return render(request,'new_connection.html')

def taskpage(request):
    return render(request,'task.html')



def task(request):
    tasks=Task.objects.all()
    form= TaskForm()

    if request.method == 'POST':
        form=TaskForm(request.POST)
        if form.is_valid():
            form.save()

            print('Tasksubmitted')

            
    contexts={'form':form,'tasks':tasks}
    return render(request , 'task.html', contexts)





def update(request,pk):
    pass 


def delete(request, pk):
    task = Task.objects.get(id=pk)
    if request.method == 'POST':
        task.delete()
        return redirect('task-page')

    context ={'item':task}
    return render(request,'delete.html', context)




total=0
def pasalpage(request):
    
    goods=Goods.objects.all()
    form=GoodsForm()
    
    if request.method == 'POST':
       form=GoodsForm(request.POST)
       
      
       if form.is_valid():
           form.save()
           quantity=form.cleaned_data.get('quantity')
           amount=form.cleaned_data.get('amount')
           global total
           total=quantity*amount
           print(total)
           print('Goodsadded')
           return redirect('pasal-page')
    
    context={'form':form,'goods':goods,'totals':total}
    return render(request , 'pasal.html',context)



def test(request):
    goods=Goods.objects.all()
    return render(request , 'test.html',{'goods':goods})

