from django import forms  
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User 
from django.forms import ModelForm
from .models import Task,Goods
from django.db import models
from .models import Worker


class CreateUserForm(UserCreationForm):
    class Meta:
        model=User
        fields=['username','email','password1','password2']


tasks=(
    ("Water pouring","Water pouring"),
    ("Solar","Solar"),
    ("geyser","geyser"),
)

class TaskForm(forms.ModelForm):
    
    class Meta:
        model=Task
        fields=['assignedto','custumername','phone','task','assignedby']
        
  
 
        
class GoodsForm(forms.ModelForm):
    total=forms.CharField()
    class Meta:
        model=Goods
        fields=['productname','amount','quantity']