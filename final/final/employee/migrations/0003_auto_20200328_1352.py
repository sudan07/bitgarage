# Generated by Django 2.2.7 on 2020-03-28 08:07

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('employee', '0002_task_assignedto'),
    ]

    operations = [
        migrations.AlterField(
            model_name='task',
            name='assignedto',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='employee.Worker'),
        ),
    ]
