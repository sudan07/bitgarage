# Generated by Django 2.2.7 on 2020-03-30 12:25

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('employee', '0011_goods'),
    ]

    operations = [
        migrations.AddField(
            model_name='goods',
            name='quantity',
            field=models.IntegerField(default=1),
        ),
        migrations.AddField(
            model_name='goods',
            name='total',
            field=models.IntegerField(default=0),
            preserve_default=False,
        ),
    ]
